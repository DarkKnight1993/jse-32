package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    @Nullable
    private String userId;

    @Nullable
    @Override
    public String getUserId() {
        return userId == null ? null : userId;
    }

    @Override
    public void setUserId(@Nullable final String userId) {
        this.userId = userId;
    }

    @Override
    public void clearUserId() {
        userId = null;
    }

    @Override
    public boolean isUserIdExists() {
        return userId == null;
    }
}
