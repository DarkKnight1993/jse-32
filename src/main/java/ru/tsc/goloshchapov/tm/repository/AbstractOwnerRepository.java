package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.IOwnerRepository;
import ru.tsc.goloshchapov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void add(@NotNull final String userId, @NotNull E entity) {
        entity.setUserId(userId);
        add(entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        remove(entity);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null) != null;
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<E> entitiesForRemoveWithUserId = findAll(userId);
        entities.removeAll(entitiesForRemoveWithUserId);
    }

}
