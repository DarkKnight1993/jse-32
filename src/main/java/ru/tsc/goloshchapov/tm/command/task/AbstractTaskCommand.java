package ru.tsc.goloshchapov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void showTask(@Nullable Task task) {
        if (task == null) return;
        System.out.println("[SELECTED TASK]");
        System.out.println(
                "Id: " + task.getId() +
                        "\nName: " + task.getName() +
                        "\nDescription: " + task.getDescription() +
                        "\nStatus: " + task.getStatus()
        );
        System.out.println("[END TASK]");
        System.out.println("[OK]");
    }

}
