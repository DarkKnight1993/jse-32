package ru.tsc.goloshchapov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {
    @NotNull
    public static final String NAME = "data-backup-load";

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return "Load data from backup";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull File file = new File(FILE_BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
