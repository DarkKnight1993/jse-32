package ru.tsc.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataJsonSaveJaxbCommand extends AbstractDataCommand {
    @Override
    public @NotNull String name() {
        return "data-save-json-jaxb";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return "Save data in json format by jaxb";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE BY JAXB]");
        System.setProperty(SYSTEM_JSON_PROPERTY_NAME, SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(JAXB_JSON_PROPERTY_NAME, JAXB_JSON_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}
