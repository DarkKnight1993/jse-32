package ru.tsc.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class AuthLogoutCommand extends AbstractAuthCommand {
    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout user from app";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
